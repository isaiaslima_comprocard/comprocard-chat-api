import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { HubConnection } from '@aspnet/signalr';
import { environment } from '../../environments/environment';
import * as signalR from '@aspnet/signalr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  nomeUsuario: string;
  private idUserLogado: number;
  idUsuarioSelecionado: number;
  private nomeUsuarioSelecionado: string;
  messageText: string;
  searchUser: string = '';

  userList: any;
  userMessages: any;

  isAlert: boolean = false;
  typeAlert: string = 'danger';
  messageAlert: string = '';

  private hubConnection: HubConnection;

  constructor(private authService: AuthService, private userService: UserService, private messageService: MessageService) {

  }

  ngOnInit() {
    this.idUserLogado = this.authService.currentUserValue.idUser;
    this.nomeUsuario = this.authService.currentUserValue.apelido;

    this.getUserList(this.searchUser);

    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(environment.urlHub)
      .build();

    this.hubConnection
      .start()
        .then(() => console.log('Conexão iniciada!'))
        .catch(err => console.log('Erro ao tentar estabelecer a conexão com o servidor!'));

    this.hubConnection.on('updateMessages', (apelidoDestino: string, apelidoOrigem: string) => {
      if (apelidoDestino == this.nomeUsuario) {
        this.getUserList(this.searchUser);
        if (this.idUsuarioSelecionado != undefined && this.nomeUsuarioSelecionado == apelidoOrigem) {
          this.getMessageByUsuario(this.idUsuarioSelecionado);
        }
      }
    });

    this.hubConnection.on('registerError', (message: string) => {
      console.log(message);
    });

    this.hubConnection.on('updateConfirmReadMessage', (apelido: string, apelidoDestino: string) => {
      if (apelidoDestino == this.nomeUsuario) {
        this.getUserList(this.searchUser);
        if (this.idUsuarioSelecionado != undefined && this.nomeUsuarioSelecionado == apelido) {
          this.getMessageByUsuario(this.idUsuarioSelecionado);
        }
      }
    });
  }

  getUserList(search) {
    this.userService.getUserList(search)
      .subscribe(
        data => {
          this.userList = data;
        },
        err => {
          console.log(err);
        }
      );
  }

  setUsuarioSelecionado(idUsuarioSelecionado, usuarioSelecionado) {
    this.nomeUsuarioSelecionado = usuarioSelecionado;
    this.idUsuarioSelecionado = idUsuarioSelecionado;
    this.getMessageByUsuario(idUsuarioSelecionado);
  }

  getMessageByUsuario(idUser) {
    this.messageService.getMessage(idUser)
      .subscribe(
        data => {
          this.userMessages = data;

          this.getUserList(this.searchUser);

          this.hubConnection.invoke('UpdateConfirmReadMessage', this.nomeUsuario, this.nomeUsuarioSelecionado)
            .catch(err => console.log(err));
        },
        err => {
          console.log(err);
        }
      );
  }

  sendMessage() {
    if (this.messageText.trim() != '') {
      this.hubConnection.invoke('SendMessage', this.nomeUsuario, this.nomeUsuarioSelecionado, this.messageText)
        .then(() => {
          this.messageText = '';
          this.getUserList(this.searchUser);
          if (this.idUsuarioSelecionado != undefined) {
            this.getMessageByUsuario(this.idUsuarioSelecionado);
          }
        })
        .catch(err => console.log(err));
    }
    else {
      this.sendAlert('info', 'É preciso digitar uma mensagem antes de enviar!');
    }
  }

  sendAlert(type, message) {
    this.messageAlert = message;
    this.typeAlert = type;
    this.isAlert = true;
  }

  dismissAlert() {
    this.isAlert = false;
  }
}
