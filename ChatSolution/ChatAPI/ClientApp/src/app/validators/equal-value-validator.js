"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** this control value must be equal to given control's value */
function equalValueValidator(targetKey, toMatchKey) {
    return function (group) {
        var target = group.controls[targetKey];
        var toMatch = group.controls[toMatchKey];
        if (target.touched && toMatch.touched) {
            var isMatch = target.value === toMatch.value;
            // set equal value error on dirty controls
            if (!isMatch && target.valid && toMatch.valid) {
                toMatch.setErrors({ equalValue: targetKey });
                var message = targetKey + ' != ' + toMatchKey;
                return { 'equalValue': message };
            }
            if (isMatch && toMatch.hasError('equalValue')) {
                toMatch.setErrors(null);
            }
        }
        return null;
    };
}
exports.equalValueValidator = equalValueValidator;
//# sourceMappingURL=equal-value-validator.js.map