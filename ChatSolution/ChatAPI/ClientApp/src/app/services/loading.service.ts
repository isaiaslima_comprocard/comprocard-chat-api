import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export class OverlayLoading {
  show: boolean = false;

  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private overlayLoadingObjectSource = new BehaviorSubject<OverlayLoading>(new OverlayLoading());
  currentLoadingObject = this.overlayLoadingObjectSource.asObservable();

  constructor() { }

  changeOverlayLoading(overlayLoadingObject: OverlayLoading) {
    this.overlayLoadingObjectSource.next(overlayLoadingObject);
  }
}
