import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor(private http: HttpClient) {

  }

  getMessage(idUserSelecionado: number) {
    return this.http.get(`${environment.urlAPI}Messages/GetMessages?idUserDestino=${idUserSelecionado}`)
      .pipe(map(messagesList => {
        return messagesList;
      }))
  }
}
