import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import { RegisterUser } from '../models/register-user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {

  }

  registerUser(registerUser: RegisterUser) {
    return this.http.post<any>(`${environment.urlAPI}Users/Register`, registerUser)
      .pipe(map(registerUser => {
        return registerUser;
      }));
  }

  getUserList(search) {
    return this.http.get<any>(`${environment.urlAPI}Users/GetUsersList?search=${search}`)
      .pipe(map(userList => {
        return userList;
      }))
  }
}
