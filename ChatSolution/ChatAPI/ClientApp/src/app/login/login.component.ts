import { Component } from "@angular/core";
import { AuthService } from "../auth.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { OverlayLoading, LoadingService } from "../services/loading.service";
import { HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  overlayLoading: OverlayLoading = {
    show: true
  };

  public form: FormGroup;
  public returnUrl: string;

  public isAlert: boolean = false;
  public alertMessage: string;

  constructor(public fb: FormBuilder, public router: Router, private route: ActivatedRoute, private authService: AuthService, private loadingService: LoadingService) {
    this.form = this.fb.group({
      'apelido': [null, Validators.compose([Validators.required])],
      'senha': [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  private getApelido(): any { return this.form.get('apelido').value }
  private getSenha(): any { return this.form.get('senha').value }

  public onSubmit(values: Object): void {
    if (this.form.valid) {
      this.loadingService.changeOverlayLoading({
        show: true
      });

      this.authService.login(this.getApelido(), this.getSenha())
        .subscribe(
          data => {
            this.router.navigate([this.returnUrl]);

            this.loadingService.changeOverlayLoading({
              show: false
            });
          },
          error => {
            this.isAlert = true;
            this.alertMessage = error.message;

            this.loadingService.changeOverlayLoading({
              show: false
            });
          }
        );
    }
  }

  public dismissAlert() {
    this.isAlert = false;
  }

  public registerUser() {
    this.router.navigate(['register']);
  }
}
