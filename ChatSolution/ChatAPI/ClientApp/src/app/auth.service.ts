import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import { User } from './models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  
  constructor(private http: HttpClient, private cookieService: CookieService) {
    if (cookieService.get('ComprocardChatUser').length > 0) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(cookieService.get('ComprocardChatUser')));
      this.currentUser = this.currentUserSubject.asObservable();
    }
    else {
      this.currentUserSubject = new BehaviorSubject<User>(null);
      this.currentUser = this.currentUserSubject.asObservable();
    }
  }

  login(apelido: string, password: string) {
    var login = {
      Apelido: apelido,
      Senha: password
    };

    return this.http.post<any>(`${environment.urlAPI}Users/Login`, login)
      .pipe(map(user => {
        if (user && user.token) {
          this.cookieService.set('ComprocardChatUser', JSON.stringify(user));
          this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(this.cookieService.get('ComprocardChatUser')));
          this.currentUser = this.currentUserSubject.asObservable();
        }

        return user;
      }));
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  logout() {
    this.currentUserSubject.next(null);
    this.cookieService.delete('ComprocardChatUser');
  }
}
