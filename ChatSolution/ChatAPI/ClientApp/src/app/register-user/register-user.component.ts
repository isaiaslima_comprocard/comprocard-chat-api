import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/user.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { equalValueValidator } from "../validators/equal-value-validator";
import { RegisterUser } from "../models/register-user";

@Component({
  templateUrl: './register-user.component.html',
  selector: 'app-register-user',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {
  public form: FormGroup;

  constructor(public fb: FormBuilder, public router: Router, private route: ActivatedRoute, private userService: UserService) {
    this.form = this.fb.group({
      'idUser': [null],
      'nome': [null, Validators.compose([Validators.required])],
      'apelido': [null, Validators.compose([Validators.required])],
      'senha': [null, Validators.compose([Validators.required])],
      'confirmaSenha': [null, Validators.compose([Validators.required])],
    }, {
      validator: equalValueValidator('senha', 'confirmaSenha')
    });
  }

  ngOnInit() {

  }

  private getRegisterUser(): RegisterUser {
    var user = new RegisterUser();
    user.idUser = 0;
    user.nome = this.form.get('nome').value;
    user.apelido = this.form.get('apelido').value;
    user.senha = this.form.get('senha').value;

    return user;
  }

  onSubmit(values: object) {
    if (this.form.valid) {
      var user = this.getRegisterUser();
      this.userService.registerUser(user)
        .subscribe(
          data => {
            if (data.idUser > 0) {
              this.router.navigate(['login']);
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  cancelar() {
    this.router.navigate(['login']);
  }
}
