import { Component, OnInit, Input } from '@angular/core';
import { LoadingService, OverlayLoading } from '../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  overlayLoading: OverlayLoading = {
    show: false
  }

  constructor(private loadingService: LoadingService) {
    this.loadingService.currentLoadingObject.subscribe(overlayLoadingObject => this.overlayLoading = overlayLoadingObject);
  }

  ngOnInit() {
  }

}
