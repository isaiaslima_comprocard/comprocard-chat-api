export const environment = {
  production: true,
  urlAPI: 'https://sistemas.comprocard.com.br/comprocardchat-api/api/',
  urlHub: 'https://sistemas.comprocard.com.br/comprocardchat-api/chat'
};
