﻿using ChatAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Context
{
    public class ChatContext : DbContext
    {
        private readonly IConfiguration _configuration = null;
        private readonly bool _isDevelopment = false;
        public ChatContext( DbContextOptions<ChatContext> options, IConfiguration configuration, IWebHostEnvironment env ) : base( options )
        {
            this._configuration = configuration;
            this._isDevelopment = env.IsDevelopment();
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Messages> Messages { get; set; }

        protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
        {
            if( _isDevelopment )
            {
                optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer( @$"{_configuration.GetConnectionString( "ChatContext" )}" );
            }
            else
            {
                optionsBuilder
                .UseLazyLoadingProxies();
            }
        }

        protected override void OnModelCreating( ModelBuilder modelBuilder )
        {
            modelBuilder.Entity<Messages>()
                .HasOne( e => e.User )
                .WithMany( c => c.Messages )
                .HasForeignKey( "IdUser" )
                .OnDelete( DeleteBehavior.NoAction )
                .IsRequired();

            modelBuilder.Entity<Messages>()
                .HasOne( e => e.UserDestino )
                .WithMany( c => c.MessagesDestino )
                .HasForeignKey( "IdUserDestino" )
                .OnDelete( DeleteBehavior.NoAction )
                .IsRequired();
        }
    }
}