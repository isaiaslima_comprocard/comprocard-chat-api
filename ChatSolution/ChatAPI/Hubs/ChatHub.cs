﻿using ChatAPI.Models;
using ChatAPI.Services.Interface;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Hubs
{
    public class ChatHub : Hub
    {
        public readonly IUserService _userService = null;
        public readonly IMessageService _messageService = null;

        public ChatHub( IUserService userService,  IMessageService messageService )
        {
            this._userService = userService;
            this._messageService = messageService;
        }

        public async Task SendMessage( string apelido, string apelidoDestino, string message )
        {
            try
            {
                Users _user = _userService.GetUsersByApelido( apelido );
                Users _userDestino = _userService.GetUsersByApelido( apelidoDestino );

                if( _user != null && _userDestino != null )
                {
                    Messages _message = new Messages();
                    _message.IdMessage = 0;
                    _message.IdUser = _user.IdUser;
                    _message.IdUserDestino = _userDestino.IdUser;
                    _message.DataHora = DateTime.Now;
                    _message.Message = message;

                    _message = _messageService.SaveMessage( _message );

                    await Clients.All.SendAsync( "updateMessages", _userDestino.Apelido, _user.Apelido );
                }
                else
                {
                    await Clients.All.SendAsync( "registerError", "Usuário não existe!" );
                }
            }
            catch( Exception _erro )
            {
                await Clients.All.SendAsync( "registerError", "Erro ao tentar enviar a mensagem!" );
            }
        }

        public async Task UpdateConfirmReadMessage( string apelido, string apelidoDestino )
        {
            await Clients.All.SendAsync( "updateConfirmReadMessage", apelido, apelidoDestino );
        }
    }
}