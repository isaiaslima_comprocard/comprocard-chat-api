﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models.DTO
{
    public class UserLoginDTO
    {
        public Int64 IdUser { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public string Token { get; set; }
    }
}