﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models.DTO
{
    public class MessagesDTO
    {
        public Int64 IdMessage { get; set; }
        public string UserApelido { get; set; }
        public string UserApelidoDestino { get; set; }
        public DateTime DataHora { get; set; }
        public string Message { get; set; }
        public bool IsLida { get; set; }
    }
}
