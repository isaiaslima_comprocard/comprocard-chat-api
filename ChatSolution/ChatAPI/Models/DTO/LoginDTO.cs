﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models.DTO
{
    public class LoginDTO
    {
        public string Apelido { get; set; }
        public string Senha { get; set; }
    }
}