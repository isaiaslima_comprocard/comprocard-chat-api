﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models.DTO
{
    public class UserListDTO
    {
        public Int64 IdUser { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public int MensagensNaoLidas { get; set; }
    }
}
