﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models
{
    public class Messages
    {
        [Key]
        public Int64 IdMessage { get; set; }
        public Int64 IdUser { get; set; }
        public Int64 IdUserDestino { get; set; }
        public DateTime DataHora { get; set; }
        public string Message { get; set; }
        public byte IsLida { get; set; }

        public virtual Users User { get; set; }

        public virtual Users UserDestino { get; set; }
    }
}