﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Models
{
    public class Users
    {
        [Key]
        public Int64 IdUser { get; set; }

        [MaxLength(120)]
        [Required]
        public string Nome { get; set; }

        [MaxLength(50)]
        [Required]
        public string Apelido { get; set; }

        [MaxLength(200)]
        [Required]
        public string Senha { get; set; }

        public virtual ICollection<Messages> Messages { get; set; }
        public virtual ICollection<Messages> MessagesDestino { get; set; }
    }
}
