﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ChatAPI.Models.DTO;
using ChatAPI.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChatAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IMessageService _messageService = null;

        public MessagesController( IMessageService mesageService )
        {
            this._messageService = mesageService;
        }

        [HttpGet]
        [ActionName( "GetMessages" )]
        public ActionResult<List<MessagesDTO>> GetMessages( int idUserDestino )
        {
            try
            {
                string _idUserStr = GetTokenInfo( "UserID" );
                int _idUser = Convert.ToInt32( _idUserStr );

                _messageService.MarkReadMessage( _idUser, idUserDestino );

                List<MessagesDTO> _messages = ( from m in _messageService.GetMessagesByIdUser( _idUser, idUserDestino )
                                                select new MessagesDTO
                                                {
                                                    IdMessage = m.IdMessage,
                                                    UserApelido = m.User.Apelido,
                                                    UserApelidoDestino = m.UserDestino.Apelido,
                                                    DataHora = m.DataHora,
                                                    Message = m.Message,
                                                    IsLida = ( m.IsLida == 1 )
                                                } ).ToList();

                return Ok( _messages );
            }
            catch( Exception _erro )
            {
                return StatusCode( StatusCodes.Status500InternalServerError, "Erro ao tentar listar os usuários!" );
            }
        }

        private string GetTokenInfo(string key)
        {
            string _authorization = Request.Headers["Authorization"].FirstOrDefault();
            _authorization = _authorization.Replace( "Bearer ", string.Empty );
            var _handler  = new JwtSecurityTokenHandler();
            if( _handler.CanReadToken( _authorization ) )
            {
                var _token       = _handler.ReadJwtToken( _authorization );
                var _claimsToken = _token.Claims;
                foreach( Claim _claim in _claimsToken )
                {
                    if( key.ToLower() == _claim.Type.ToLower() )
                    {
                        return _claim.Value;
                    }
                }

                return string.Empty;
            }

            throw new Exception( "Token não pode ser lido!" );
        }
    }
}