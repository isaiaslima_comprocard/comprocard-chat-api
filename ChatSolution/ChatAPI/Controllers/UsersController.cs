﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using ChatAPI.Code.Api;
using ChatAPI.Models;
using ChatAPI.Models.DTO;
using ChatAPI.Security;
using ChatAPI.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace ChatAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class UsersController : AngularControllerBase
    {
        private readonly IUserService _userService = null;
        private readonly IMessageService _messageService = null;

        public UsersController( IUserService userService, IMessageService messageService )
        {
            this._userService = userService;
            this._messageService = messageService;
        }

        [AllowAnonymous]
        [HttpPost]
        [ActionName( "Login" )]
        public ActionResult<UserLoginDTO> Login( [FromBody] LoginDTO login, 
            [FromServices] SigningConfigurations signingConfigurations,
            [FromServices] TokenConfigurations tokenConfigurations )
        {
            try
            {
                Users _user = _userService.GetUsersByApelido( login.Apelido );
                if( _user != null )
                {
                    if( _user.Senha == CriptografiaService.EncryptHash( login.Senha ) )
                    {
                        ClaimsIdentity identity = new ClaimsIdentity(
                            new GenericIdentity(_user.Apelido, "Login"),
                            new[] {
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                                new Claim(JwtRegisteredClaimNames.UniqueName, _user.IdUser.ToString()),
                                new Claim("UserID", _user.IdUser.ToString())
                            }
                        );

                        DateTime dataCriacao = DateTime.Now;
                        DateTime dataExpiracao = dataCriacao +
                            TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                        var handler = new JwtSecurityTokenHandler();
                        var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                        {
                            Issuer = tokenConfigurations.Issuer,
                            Audience = tokenConfigurations.Audience,
                            SigningCredentials = signingConfigurations.SigningCredentials,
                            Subject = identity,
                            NotBefore = dataCriacao,
                            Expires = dataExpiracao
                        });
                        var _token = handler.WriteToken(securityToken);


                        return Ok( new UserLoginDTO
                        {
                            IdUser = _user.IdUser,
                            Apelido = _user.Apelido,
                            Nome = _user.Nome,
                            Token = _token
                        } );
                    }

                    return StatusCodeAngular( StatusCodes.Status400BadRequest, "Senha não confere!" );
                }

                return StatusCodeAngular( StatusCodes.Status404NotFound, "Usuário não existe!" );
            }
            catch( Exception _erro )
            {
                return StatusCodeAngular( StatusCodes.Status500InternalServerError, "Erro ao tentar efetuar o login!" );
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [ActionName( "Register" )]
        public ActionResult<UserDTO> Register( [FromBody] UserDTO user )
        {
            try
            {
                if( user.IdUser == 0 )
                {
                    Users _user = new Users
                    {
                        Nome = user.Nome,
                        Apelido = user.Apelido,
                        Senha = CriptografiaService.EncryptHash( user.Senha )
                    };

                    _user = _userService.Save( _user );

                    user.IdUser = _user.IdUser;

                    return Ok( user );
                }

                return BadRequest();
            }
            catch( Exception _erro )
            {
                return StatusCodeAngular( StatusCodes.Status500InternalServerError, "Erro ao tentar adicionar um novo usuário!" );
            }
        }

        [HttpPut]
        [ActionName( "Edit" )]
        public ActionResult<UserDTO> Edit( [FromBody] UserDTO user )
        {
            try
            {
                if( user.IdUser > 0 )
                {
                    Users _user = new Users
                    {
                        IdUser = user.IdUser,
                        Nome = user.Nome,
                        Apelido = user.Apelido,
                        Senha = CriptografiaService.EncryptHash( user.Senha )
                    };

                    _user = _userService.Save( _user );

                    return Ok( user );
                }

                return StatusCodeAngular( StatusCodes.Status400BadRequest, "Usuário pode não existir!" );
            }
            catch( Exception _erro )
            {
                return StatusCodeAngular( StatusCodes.Status500InternalServerError, "Erro ao tentar adicionar um novo usuário!" );
            }
        }

        [HttpGet]
        [ActionName( "GetUsersList" )]
        public ActionResult<List<UserListDTO>> GetUsers( string search )
        {
            try
            {
                string _idUserStr = GetTokenInfo( "UserID" );
                int _idUser = Convert.ToInt32( _idUserStr );

                List<UserListDTO> _usersList = ( from u in _userService.GetUsersList( _idUser, search )
                                                 select new UserListDTO
                                                 {
                                                     IdUser = u.IdUser,
                                                     Nome = u.Nome,
                                                     Apelido = u.Apelido,
                                                     MensagensNaoLidas = _messageService.GetMessagesByIdUser( _idUser, u.IdUser )
                                                        .Where( m => m.IdUserDestino == _idUser && m.IsLida == 0 )
                                                        .Count()
                                                 } ).ToList();

                return Ok( _usersList );
            }
            catch( Exception _erro )
            {
                return StatusCodeAngular( StatusCodes.Status500InternalServerError, "Erro ao tentar listar os usuários!" );
            }
        }

        private string GetTokenInfo(string key)
        {
            string _authorization = Request.Headers["Authorization"].FirstOrDefault();
            _authorization = _authorization.Replace( "Bearer ", string.Empty );
            var _handler  = new JwtSecurityTokenHandler();
            if( _handler.CanReadToken( _authorization ) )
            {
                var _token       = _handler.ReadJwtToken( _authorization );
                var _claimsToken = _token.Claims;
                foreach( Claim _claim in _claimsToken )
                {
                    if( key.ToLower() == _claim.Type.ToLower() )
                    {
                        return _claim.Value;
                    }
                }

                return string.Empty;
            }

            throw new Exception( "Token não pode ser lido!" );
        }
    }
}