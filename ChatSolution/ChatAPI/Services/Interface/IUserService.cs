﻿using ChatAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Services.Interface
{
    public interface IUserService
    {
        Users GetUsersById( int idUser );
        Users GetUsersByApelido( string apelido );
        List<Users> GetUsersList( int idUserIgnore, string search );
        Users Save( Users user );
        void Delete( int idUser );
    }
}