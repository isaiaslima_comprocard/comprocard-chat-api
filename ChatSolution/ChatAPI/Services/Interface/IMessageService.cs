﻿using ChatAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Services.Interface
{
    public interface IMessageService
    {
        List<Messages> GetMessagesByIdUser( Int64 idUser, Int64 idUserDestino );
        Messages SaveMessage( Messages message );
        void MarkReadMessage( int idUser, int idUserDestino );
        void DeleteMessage( int idMessage );
    }
}
