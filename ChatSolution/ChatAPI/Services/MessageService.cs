﻿using ChatAPI.Context;
using ChatAPI.Models;
using ChatAPI.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Services
{
    public class MessageService : IMessageService
    {
        private readonly ChatContext _context = null;

        public MessageService( ChatContext context )
        {
            this._context = context;
        }

        public List<Messages> GetMessagesByIdUser(Int64 idUser, Int64 idUserDestino)
        {
            List<Messages> _messagesList = _context.Messages.Where( p => ( p.IdUser == idUser || p.IdUserDestino == idUser ) && ( p.IdUser == idUserDestino || p.IdUserDestino == idUserDestino ) )
                .ToList();

            return _messagesList;
        }

        public Messages SaveMessage( Messages message )
        {
            if( message.IdMessage == 0 )
            {
                _context.Messages.Add( message );

                _context.SaveChanges();
            }

            return message;
        }

        public void MarkReadMessage( int idUser, int idUserDestino )
        {
            List<Messages> _messages = _context.Messages.Where( p => p.IdUserDestino == idUser && p.IdUser == idUserDestino )
                .ToList();
            foreach( Messages _message in _messages )
            {
                _message.IsLida = 1;
                _context.Attach( _message );
                _context.Entry<Messages>( _message ).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            }

            _context.SaveChanges();
        }

        public void DeleteMessage(int idMessage)
        {
            Messages _message = _context.Messages.FirstOrDefault( p => p.IdMessage == idMessage );
            if( _message != null )
            {
                _context.Remove( _message );
                _context.SaveChanges();
            }
        }        
    }
}