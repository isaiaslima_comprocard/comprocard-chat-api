﻿using ChatAPI.Context;
using ChatAPI.Models;
using ChatAPI.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Services
{
    public class UserService : IUserService
    {
        private readonly ChatContext _context = null;
        public UserService( ChatContext context )
        {
            this._context = context;
        }


        public Users GetUsersByApelido(string apelido)
        {
            Users _user = _context.Users.FirstOrDefault( p => p.Apelido == apelido );

            return _user;
        }

        public Users GetUsersById( int idUser )
        {
            Users _user = _context.Users.Find( idUser );

            return _user;
        }

        public List<Users> GetUsersList( int idUserIgnore, string search )
        {
            List<Users> _userList = _context.Users.Where( p => p.IdUser != idUserIgnore && 
                                                               ( p.Nome.Contains( search ) || 
                                                                 p.Apelido.Contains( search ) ||
                                                                 string.IsNullOrEmpty( search ) ) )
                .ToList();

            return _userList;
        }

        public Users Save(Users user)
        {
            if( user.IdUser == 0 )
            {
                _context.Users.Add( user );
            }
            else
            {
                _context.Users.Attach( user );
                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            }

            _context.SaveChanges();

            return user;
        }
        public void Delete(int idUser)
        {
            Users _user = this.GetUsersById( idUser );
            if( _user != null )
            {
                foreach( Messages _message in _user.Messages )
                {
                    _context.Remove( _message );
                }

                _context.Remove( _user );
            }
        }
    }
}