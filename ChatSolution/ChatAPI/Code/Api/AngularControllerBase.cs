﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAPI.Code.Api
{
    public class AngularControllerBase : ControllerBase
    {
        [NonAction]
        public virtual ObjectResult StatusCodeAngular( int statusCode, string message )
        {
            return StatusCode( statusCode, 
                new
                {
                    message = new
                    {
                        status = statusCode,
                        message = message
                    }
                } );
        }
    }
}
