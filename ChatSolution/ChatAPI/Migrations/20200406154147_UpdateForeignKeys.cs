﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAPI.Migrations
{
    public partial class UpdateForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Users_UserDestinoIdUser",
                table: "Messages");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Users_UserIdUser",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_UserDestinoIdUser",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_UserIdUser",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "UserDestinoIdUser",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "UserIdUser",
                table: "Messages");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_IdUser",
                table: "Messages",
                column: "IdUser");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_IdUserDestino",
                table: "Messages",
                column: "IdUserDestino");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Users_IdUser",
                table: "Messages",
                column: "IdUser",
                principalTable: "Users",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Users_IdUserDestino",
                table: "Messages",
                column: "IdUserDestino",
                principalTable: "Users",
                principalColumn: "IdUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Users_IdUser",
                table: "Messages");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Users_IdUserDestino",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_IdUser",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_IdUserDestino",
                table: "Messages");

            migrationBuilder.AddColumn<long>(
                name: "UserDestinoIdUser",
                table: "Messages",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "UserIdUser",
                table: "Messages",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Messages_UserDestinoIdUser",
                table: "Messages",
                column: "UserDestinoIdUser");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_UserIdUser",
                table: "Messages",
                column: "UserIdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Users_UserDestinoIdUser",
                table: "Messages",
                column: "UserDestinoIdUser",
                principalTable: "Users",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Users_UserIdUser",
                table: "Messages",
                column: "UserIdUser",
                principalTable: "Users",
                principalColumn: "IdUser");
        }
    }
}
